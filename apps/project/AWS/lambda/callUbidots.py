'''
Created on Dec 7, 2019

@author: shaoshengyin
'''
from ubidots import ApiClient
from ubidots import UbidotsError400
from ubidots import UbidotsForbiddenError

def lambda_handler(event, context):
    # initialize Api client
    ubidotsToken = event['token']
    api = ApiClient(token=str(ubidotsToken),
                    base_url="https://industrial.api.ubidots.com/api/v1.6/")

    # using id to get correspond parameter
    try:
        my_variable = api.get_variable('5dc63c0c4763e77145ba1d2a')
    except UbidotsError400 as e:
        print(e.message)
        print(e.detail)
    except UbidotsForbiddenError as e:
        print("For some reason my account does not have permission to read this variable")
        print(e.message)
        print(e.detail)

    # save value by calling api
    curTemp = event['curValue']
    event.pop('token')
    new_value = my_variable.save_value({'value': curTemp,'context':event})
    print(new_value)

    return "success"