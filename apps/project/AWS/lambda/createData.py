'''
Created on Dec 7, 2019

@author: shaoshengyin
'''
import decimal
import boto3
import json
def round_float_to_decimal(float_value):
    """
    Convert a floating point value to a decimal that DynamoDB can store,
    and allow rounding.
    """

    # Perform the conversion using a copy of the decimal context that boto3
    # uses. Doing so causes this routine to preserve as much precision as
    # boto3 will allow.
    with decimal.localcontext(boto3.dynamodb.types.DYNAMODB_CONTEXT) as \
         decimalcontext:

        # Allow rounding. 8 bits precision
        decimalcontext.prec = 8
        decimalcontext.traps[decimal.Inexact] = 0
        decimalcontext.traps[decimal.Rounded] = 0
        decimal_value = decimalcontext.create_decimal_from_float(float_value)
        return decimal_value

def lambda_handler(event, context):
    dynamodb = boto3.resource('dynamodb')
    lambda_client = boto3.client('lambda')
    table = dynamodb.Table('TempSensor')
    sampleCount = round_float_to_decimal(event['sampleCount'])
    avgValue = round_float_to_decimal(event['avgValue'])
    minValue = round_float_to_decimal(event['minValue'])
    maxValue = round_float_to_decimal(event['maxValue'])
    totValue = round_float_to_decimal(event['totValue'])
    curValue = round_float_to_decimal(event['curValue'])
    table.put_item(
        Item={
            "name": event['name'],
            "timeStamp": event['timeStamp'],
            "avgValue": avgValue,
            "minValue": minValue,
            "maxValue": maxValue,
            "curValue": curValue,
            "totValue": totValue,
            "sampleCount": sampleCount
        }
    )
    print('finish put_item')
    print('call Ubidots')
    print(event)
    event['token'] = 'BBFF-NLpXWIbbc1A74Ju8Dq1azc1pWIz35Z'
    print(event)
    invoke_response = lambda_client.invoke(FunctionName="callUbidots",
                                          InvocationType='RequestResponse',
                                          Payload=json.dumps(event)
                                          )
    data = invoke_response['Payload'].read()
    print(data)
    return 'success'
