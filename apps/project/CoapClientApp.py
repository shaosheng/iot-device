'''
Created on Oct 28, 2019

@author: shaoshengyin
'''
import sys
sys.path.append('/home/pi/yin/iot-device/apps')
from labs.common.SensorData import SensorData
from labs.common.DataUtil import DataUtil
from coapthon.client.helperclient import HelperClient
from time import sleep

from sense_hat import SenseHat
sense = SenseHat()

# initialize the host, post and path
host = "192.168.99.190"
port = 5683
path = 'temp'

# initialize sensorData and DataUtil
#sensorData = SensorData()
sensorData = SensorData()
sensorData2 = SensorData()
dataUtil = DataUtil()

#configure value
#todo: get value from RPI
#The Sense HAT gives you the humidity measurement as relative humidity. This is why the humidity sensor also has a temperature sensor built in

#add loop to get data and put/post to java server
#sensorData.addValue(20.1)
#sensorData.setName("name")
#transform to json
dataJson = dataUtil.toJson(sensorData.name, sensorData.timeStamp, sensorData.avgValue, sensorData.minValue, sensorData.maxValue, sensorData.curValue, sensorData.totValue, sensorData.sampleCount)
print("Json format of sensorData: " + dataJson)

#set up connection
client = HelperClient(server=(host, port))
print("Ping Successful!")


#test post method for one time
#response = client.post(path, dataJson)
#print("Testing post handler")
#print(response.pretty_print())

# post periodlly
while (True):
     #sensorData from RPI
     #sensorData to json
    h = sense.get_humidity();
    sensorData.addValue(h)
    sensorData.setName("relative humidity")
    t = sense.get_temperature();
    sensorData2 = SensorData();
    sensorData2.addValue(t);
    sensorData2.setName("temperature")
    
    dataJson = dataUtil.toJson(sensorData.name, sensorData.timeStamp, sensorData.avgValue, sensorData.minValue, sensorData.maxValue, sensorData.curValue, sensorData.totValue, sensorData.sampleCount) 
    response = client.post(path, dataJson)
    #dataJson = dataUtil.toJson(sensorData2.name, sensorData2.timeStamp, sensorData2.avgValue, sensorData2.minValue, sensorData2.maxValue, sensorData2.curValue, sensorData2.totValue, sensorData2.sampleCount) 
    #response = client.post(path, dataJson)
    msg = str(response.pretty_print())
    print(msg)
    #sense.show_message(msg, scroll_speed=0.05)
    sleep(60)
    

client.stop()