'''
Created on Sep 26, 2019

@author: shaoshengyin
'''
import smtplib
import logging
from project.ConfigUtil import ConfigUtil
from labbenchstudios.common import ConfigConst
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText 

class SmtpClientConnector:
    
    ''' 
    Reference Professor code
    '''

    def __init__(self):
        self.config = ConfigUtil('../../config/ConnectedDevicesConfig.props')
        self.config.loadConfig()
        logging.info('Configuration data...\n' + str(self.config))
    
    def publicMessage(self, topic, data):
        host = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.HOST_KEY)
        port = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.PORT_KEY)
        fromAddr = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.FROM_ADDRESS_KEY)
        toAddr = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.TO_ADDRESS_KEY)
        authToken = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.USER_AUTH_TOKEN_KEY)

        msg = MIMEMultipart()
        msg['From'] = fromAddr
        msg['To'] = toAddr
        msg['subject'] = topic
        msgBody = str(data)
        msg.attach(MIMEText(msgBody))
        msgText = msg.as_string()
        
        # send e-mail notification
        smtpServer = smtplib.SMTP_SSL(host, port)
        smtpServer.ehlo()
        smtpServer.login(fromAddr, authToken)
        smtpServer.sendmail(fromAddr, toAddr, msgText)
        smtpServer.close()
        
        