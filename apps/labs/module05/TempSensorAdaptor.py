'''
Created on Oct 1, 2019

@author: shaoshengyin
'''
import json
import threading
from labs.common.SensorData import SensorData
from labs.module02.SmtpClientConnector import SmtpClientConnector
from labs.common.DataUtil import DataUtil
from time import sleep
import logging
from sense_hat import SenseHat
from labs.module03 import TempActuatorEmulator
from labs.common.ActuatorData import ActuatorData

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.INFO)

class TempSensorAdaptor(threading.Thread):
    
    #default parameter
    RATE_IN_SEC = 5
    lowVal = 0
    highVal = 30
    threshold = 2
    
    #initialize object
    sensorData = SensorData()
    connector = SmtpClientConnector()
    senseHat = SenseHat();
    tempActuator = TempActuatorEmulator.TempActuatorEmulator()
    actuatorData = ActuatorData()
    
    #initialize actuatorData
    actuatorData.setValue(0)
    actuatorData.setCommand(0)
    actuatorData.setStateData(0)
    actuatorData.setErrorCode(0)
    
    '''
    @param rateInÍec: RATE_IN_SEC 
    '''
    def __init__(self, rateInSec = RATE_IN_SEC):
        super(TempSensorAdaptor, self).__init__()
        self.rateInSec = rateInSec

    def run(self):
        while True:
            if self.enableEmulator:
                #Generate temperature value
                #curTemp = uniform(float(self.lowVal), float(self.highVal))
                self.curTemp = self.senseHat.get_temperature()
                #add to SensorData
                self.sensorData.addValue(self.curTemp)
                logging.info('\n-------------')
                logging.info('new sensor data')
                logging.info('current temperature' + str(self.curTemp))
                self.diff = self.tempActuator.check(self.curTemp)
                #check current temperature is higher or lower 
                #if difference > 0 current temperature is higher we should set command as 0
                if ( self.diff > 0 ):
                    logging.info('the difference is ' + str(self.diff) + ' Need to cool down temperature')
                    self.actuatorData.setValue(abs(self.diff))
                    self.tempActuator.setMessage(self.actuatorData, 0, 0, None, 1)
                    #send actuatorData by email
                    self.tempActuator.processMessage(self.actuatorData)
                #if difference < 0 current temperature is lower we should set command as 1
                elif ( self.diff < 0 ):
                    logging.info('the difference is ' + str(self.diff) + ' Need to warm up temperature')
                    self.actuatorData.setValue(abs(self.diff))
                    self.tempActuator.setMessage(self.actuatorData, 1, 0, None, 1)
                    #send actuatorData by email
                    self.tempActuator.processMessage(self.actuatorData)
                else:
                    logging('nothing change')
                #if exceeds threshold then send email
                if ( abs(self.curTemp - self.sensorData.getAvgValue()) >= self.threshold ):
                    logging.info('Current value exceeds threshold of average value' + str(self.diff))
                    #write data to file, if file dosen't exist, then create
                    with open("/Users/shaoshengyin/Desktop/result.json", "w") as f:
                        json.dump(self.sensorData._dic_(), f)
                        #send json by email
                        self.connector.publicMessage("latest sensor data", self.sensorData._dic_())
                sleep(self.rateInSec)
                
                