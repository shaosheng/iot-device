'''
Created on Nov 9, 2019

@author: shaoshengyin
reference https://github.com/ubidots/ubidots-python
'''
import paho.mqtt.client as mqttClient
import time
import json
import ssl
from paho.mqtt import subscribe
#raspberry use only
#from labs.module03.SenseHatLedActivator import SenseHatLedActivator

'''
global variables
'''

connected = False  # Stores the connection status
BROKER_ENDPOINT = "industrial.api.ubidots.com"
TLS_PORT = 8883  # Secure port
MQTT_USERNAME = "BBFF-Ar9KUbldO2lbD32mwc65DeFlDfjgZx"  # Put here your Ubidots TOKEN
MQTT_PASSWORD = ""  # Leave this in blank
TOPIC = "/v1.6/devices/"
DEVICE_LABEL = "csye6510_yin/tempactuator"
TLS_CERT_PATH = "/Users/shaoshengyin/git/csye6510/iot-gateway/config/ubidots_cert.pem"  # Put here the path of your TLS cert

host = "industrial.ubidots.com"

#raspberry use only
#senseHat     = SenseHatLedActivator()


#override on_connect method
def on_connect(client, userdata, flags, resultCode):
    if resultCode == 0:

        print("[INFO] Connected to broker")
        global connected  # Use global variable
        connected = True  # Signal connection
    else:
        print("[INFO] Error, connection failed")
    
    
    topic = "{}{}".format(TOPIC, DEVICE_LABEL)
    client.subscribe(topic, 2)

#override on_message method
def on_message(client, userdata, msg):
    payload = str(msg.payload)                                                      
    print("Received Publish message on topic {0}. ".format(str(msg.topic)))
    if not payload:
        return
    # delete extra signs of message
    payloadtrip = payload.lstrip("b")
    newString = payloadtrip.lstrip("'").rstrip("'")                    
    print("Received message:")
    print(newString)
    #display led, raspberry use only
    #senseHat.setDisplayMessage(newString)


def on_publish(client, userdata, result):
    print("Published!")

#using TLS when connecting to ubidots    
def connect(mqtt_client, mqtt_username, mqtt_password, broker_endpoint, port):
    global connected

    if not connected:
        mqtt_client.username_pw_set(mqtt_username, password=mqtt_password)
        mqtt_client.on_connect = on_connect
        mqtt_client.on_publish = on_publish
        mqtt_client.tls_set(ca_certs=TLS_CERT_PATH, certfile=None,
                            keyfile=None, cert_reqs=ssl.CERT_REQUIRED,
                            tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)
        mqtt_client.tls_insecure_set(False)
        mqtt_client.connect(broker_endpoint, port=port)
        mqtt_client.loop_start()

        attempts = 0

        while not connected and attempts < 5:  # Wait for connection
            print(connected)
            print("Attempting to connect...")
            time.sleep(1)
            attempts += 1

    if not connected:
        print("[ERROR] Could not connect to broker")
        return False

    return True
#rewrite publish method, log error
def publish(mqtt_client, topic, payload):

    try:
        mqtt_client.publish(topic, payload)

    except Exception as e:
        print("[ERROR] Could not publish data, error: {}".format(e))

#main method, connect to broker and keep listening message
def run():
    topic = "{}{}".format(TOPIC, DEVICE_LABEL)
    mqtt_client = mqttClient.Client()
    mqtt_client.on_connect = on_connect
    mqtt_client.on_message = on_message
    connect(mqtt_client, MQTT_USERNAME,MQTT_PASSWORD, BROKER_ENDPOINT, TLS_PORT)
    mqtt_client.loop_forever()
  
    
if __name__ == '__main__':  
    run()
