'''
Created on Nov 9, 2019

@author: shaoshengyin
'''
import paho.mqtt.client as mqttClient
import time
import json
import ssl
from random import uniform

'''
global variables
'''

connected = False  # Stores the connection status
BROKER_ENDPOINT = "industrial.api.ubidots.com"
TLS_PORT = 8883  # Secure port
MQTT_USERNAME = "BBFF-uhDtFb6UuNlmibMFspKhl9o4TefQg2"  # Put here your Ubidots TOKEN
MQTT_PASSWORD = ""  # Leave this in blank
TOPIC = "/v1.6/devices/"
DEVICE_LABEL = "csye6510_yin/tempsensor"#the device and label of parameter
TLS_CERT_PATH = "/Users/shaoshengyin/git/csye6510/iot-gateway/config/ubidots_cert.pem"  # Put here the path of your TLS cert

host = "industrial.ubidots.com"

#override on_connect 
def on_connect(client, userdata, flags, resultCode):
    if resultCode == 0:

        print("[INFO] Connected to broker")
        global connected  # Use global variable
        connected = True  # Signal connection
    else:
        print("[INFO] Error, connection failed")

#override on_message method,retrive payload
def on_message(client, userdata, msg):
    payload = str(msg.payload)                                                      
    print("Received Publish message on topic {0}. ".format(str(msg.topic)))
    if not payload:
        return
    # delete extra signs of message
    payloadtrip = payload.lstrip("b")
    newString = payloadtrip.lstrip("'").rstrip("'")                    
    print("Received message:")
    print(newString)

#override on_publish method
def on_publish(client, userdata, result):
    print("Published!")

#connect ubidots using TLS    
def connect(mqtt_client, mqtt_username, mqtt_password, broker_endpoint, port):
    global connected

    if not connected:
        mqtt_client.username_pw_set(mqtt_username, password=mqtt_password)
        mqtt_client.on_connect = on_connect
        mqtt_client.on_publish = on_publish
        mqtt_client.tls_set(ca_certs=TLS_CERT_PATH, certfile=None,
                            keyfile=None, cert_reqs=ssl.CERT_REQUIRED,
                            tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)
        mqtt_client.tls_insecure_set(False)
        mqtt_client.connect(broker_endpoint, port=port)
        mqtt_client.loop_start()

        attempts = 0

        while not connected and attempts < 5:  # Wait for connection
            print(connected)
            print("Attempting to connect...")
            time.sleep(1)
            attempts += 1

    if not connected:
        print("[ERROR] Could not connect to broker")
        return False

    return True

#publish topic with payload
def publish(mqtt_client, topic, payload):

    try:
        mqtt_client.publish(topic, payload)

    except Exception as e:
        print("[ERROR] Could not publish data, error: {}".format(e))

#main method, create topic url and publish random data
def run():
    curTemp = uniform(10, 30)
    payload = json.dumps({"value": curTemp})
    topic = "{}{}".format(TOPIC, DEVICE_LABEL)

    mqtt_client = mqttClient.Client()

    if not connect(mqtt_client, MQTT_USERNAME,
                   MQTT_PASSWORD, BROKER_ENDPOINT, TLS_PORT):
        return False

    publish(mqtt_client, topic, payload)
    print("generate by mqtt client, current value :" + str(curTemp))

    return True
    
    
if __name__ == '__main__':  
    while True:
        run()
        time.sleep(60)  