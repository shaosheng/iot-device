'''
Created on Nov 9, 2019

@author: shaoshengyin
'''
from ubidots import ApiClient
from ubidots import UbidotsError400
from ubidots import UbidotsForbiddenError
from time import sleep
from random import uniform

#initialize Api client
api = ApiClient(token='BBFF-uhDtFb6UuNlmibMFspKhl9o4TefQg2', base_url="https://industrial.api.ubidots.com/api/v1.6/")

#using id to get correspond parameter
try:
    my_variable = api.get_variable('5dc63c0c4763e77145ba1d2a')
except UbidotsError400 as e:
    print(e.message)
    print(e.detail)
except UbidotsForbiddenError as e:
    print("For some reason my account does not have permission to read this variable")
    print(e.message)
    print(e.detail)

#save value by calling api
while True:
    #generate random data, scope(10,30)
    curTemp = uniform(10, 30)
    new_value = my_variable.save_value({'value': curTemp})
    print("generate by calling api, current value :" + str(curTemp))
    sleep(60)
