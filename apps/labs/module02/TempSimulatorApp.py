'''
Created on Sep 26, 2019

@author: shaoshengyin
'''
import logging
from time import sleep
from labs.module02 import TempSensorEmulator

tempSensorEmulator = TempSensorEmulator.TempSensorEmulator()
tempSensorEmulator.enableEmulator = True
tempSensorEmulator.daemon = True
tempSensorEmulator.start()
while True:
    sleep(5)
    pass