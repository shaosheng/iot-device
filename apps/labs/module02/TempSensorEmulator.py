'''
Created on Sep 26, 2019

@author: shaoshengyin
'''
import threading
from random import uniform
from labs.common.SensorData import SensorData
from labs.module02.SmtpClientConnector import SmtpClientConnector
from time import sleep
import logging

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.INFO)

class TempSensorEmulator(threading.Thread):
    RATE_IN_SEC = 30
    lowVal = 0
    highVal = 30
    threshold = 10
    
    #initialize object
    sensorData = SensorData()
    connector = SmtpClientConnector()
    
    def __init__(self, rateInSec = RATE_IN_SEC):
        super(TempSensorEmulator, self).__init__()
        self.rateInSec = rateInSec

    def run(self):
        while True:
            if self.enableEmulator:
                #Generate temperature value
                curTemp = uniform(float(self.lowVal), float(self.highVal))
                logging.info('current temperature' + str(curTemp))
                #add to SensorData
                self.sensorData.addValue(curTemp)
                #if exceeds threshold then send email
                self.diff = abs(curTemp - self.sensorData.getAvgValue())
                if ( self.diff >= self.threshold):
                    logging.info('Current value exceeds threshold of average value' + str(self.diff))
                    self.connector.publicMessage("latest sensor data", self.sensorData)    
                sleep(self.rateInSec)
                
                