'''
Created on Oct 11, 2019

@author: shaoshengyin
'''

'''
for any rasp pi use
import sys
sys.path.append('/home/pi/yin/iot-device/apps')
'''
from time import sleep
from labbenchstudios.common import ConfigUtil
from labbenchstudios.common import ConfigConst
import smbus
import threading

i2cBus              = smbus.SMBus(1)                  # Use I2C bus No.1 on Raspberry Pi3 +
enableControl       = 0x2D                            # Enable command    
enableMeasure       = 0x08                            # Enable command
accelAddr           = 0x1C                            # address for IMU (accelerometer)
magAddr             = 0x6A                            # address for IMU (magnetometer)
pressAddr           = 0x5C                            # address for pressure sensor
humidAddr           = 0x5F                            # address for humidity sensor
begAddr             = 0x28                            # beginning address
totBytes            = 6                               # read 1 to 6 bytes
DEFAULT_RATE_IN_SEC = 5                               # run at regular interval

'''
this is I2CSenseHatAdaptor class, used for reading data 
from the I2C buffers specified by the address for each sensor 
at regular interval
'''
class I2CSenseHatAdaptor(threading.Thread):
    rateInSec = DEFAULT_RATE_IN_SEC                   #set interval 
    
    accelerometerData = 0
    magnetometerData = 0
    pressureData = 0
    humidityData = 0
    
    '''
    init constructor, reading configuration file 
    '''
    def __init__(self):
        super(I2CSenseHatAdaptor, self).__init__()
        #get configuration path
        self.config = ConfigUtil.ConfigUtil('../../../config/ConnectedDevicesConfig.props')
        # load config
        self.config.loadConfig()
        #print details
        print('Configuration data...\n' + str(self.config))
        self.initI2CBus()

    '''
    init I2CBUS constructor 
    '''       
    def initI2CBus(self):
        print("Initializing I2C bus and enabling I2C addresses...")
        # write data for Accelrometer sensor 
        i2cBus.write_block_data(accelAddr, 0, 0)
        # write data for Magnetometer sensor 
        i2cBus.write_block_data(magAddr, 0, 0)
        # write data for Pressure sensor 
        i2cBus.write_block_data(pressAddr, 0, 0)
        # write data for Humidity sensor 
        i2cBus.write_block_data(humidAddr, 0, 0)
        # get Accelrometer sensor details
        self.AccelrometerData = i2cBus.read_i2c_block_data(accelAddr, begAddr, totBytes)
        # get Magnetometer sensor details
        self.MagnetometerData = i2cBus.read_i2c_block_data(magAddr, begAddr, totBytes)
        # get Pressure sensor details
        self.PressureData = i2cBus.read_i2c_block_data(pressAddr, begAddr, totBytes)
        # get Humidity sensor details
        self.HumidityData = i2cBus.read_i2c_block_data(humidAddr, begAddr, totBytes)
        
        # TODO: do the same for the magAddr, pressAddr, and humidAddr
        # NOTE: Reading data from the sensor will look like the following:
        self.accelerometerData = i2cBus.read_i2c_block_data(accelAddr, begAddr, totBytes)
        self.magnetometerData = i2cBus.read_i2c_block_data(magAddr, begAddr, totBytes)
        self.pressureData = i2cBus.read_i2c_block_data(pressAddr, begAddr, totBytes)
        self.humidityData = i2cBus.read_i2c_block_data(humidAddr, begAddr, totBytes)
        
        '''
        display Accelrometer sensor details function
        @param data: AccelrometerData
        '''
        def displayAccelrometerData(self, data):
            #get Accelrometer data
            self.accelrometerData = data
            # print to the monitor 
            print("Accelerometer block data:   " + str(self.accelrometerData))
            
        '''
        display Magnetometer sensor details function
        @param data: MagnetometerData
        '''
        def displayMagnetometerData(self, data):
            #get Magnetometer data
            self.magnetometerData = data
            # print to the monitor 
            print("Magnetometer block data:   " + str(self.magnetometerData))
        
        '''
        display Pressure sensor details function
        @param data: PressureData
        '''
        def displayPressureData(self, data):
            #get Pressure data
            self.pressureData = data
            # print to the monitor
            print("Pressure block data:   " + str(self.pressureData))
        
        '''
        display Humidity sensor details function
        @param data: HumidityData
        
        '''
        def displayHumidityData(self, data):
            #get Pressure data
            self.humidityData = data
            # print to the monitor
            print("Humidity block data:   " + str(self.humidityData))
        
        '''
        overwrite thread run function
        '''
        def run(self):
            while True:
                # when enableEmulator is true
                if self.enableEmulator:
                    # call displayAccelerometerData function
                    self.displayAccelerometerData(self.accelerometerData)
                    # call displayMagnetometerData function 
                    self.displayMagnetometerData(self.magnetometerData)
                    # call displayPressureData function
                    self.displayPressureData(self.pressureData)
                    # call displayHumidityData function
                    self.displayHumidityData(self.humidityData)               
                # sleep in regular seconds
                sleep(self.rateInSec)
        
    
    