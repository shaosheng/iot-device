'''
Created on Sep 26, 2019

@author: shaoshengyin
'''
import logging
from time import sleep
from labs.module04 import I2CSenseHatAdaptor
from labs.module04 import SenseHatDeviceAdaptor

# create an instance of i2csensenhatadaptor
i2csensenhatadaptor = I2CSenseHatAdaptor.I2CSenseHatAdaptor()
i2csensenhatadaptor.enableEmulator = True
i2csensenhatadaptor.daemon = True
i2csensenhatadaptor.start()

# create an instance of SenseHatDeviceAdaptor
senseHatDeviceAdaptor = SenseHatDeviceAdaptor.SenseHatDeviceAdaptor()
senseHatDeviceAdaptor.enableEmulator = True
senseHatDeviceAdaptor.daemon = True
senseHatDeviceAdaptor.start()


while True:
    sleep(5)
    pass