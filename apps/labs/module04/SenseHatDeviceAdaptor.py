'''
Created on Oct 11, 2019

@author: shaoshengyin
'''

import threading
from time import sleep
from sense_hat import SenseHat
from labs.module03 import TempActuatorEmulator


class SenseHatDeviceAdaptor(threading.Thread):
    
    #default parameter
    RATE_IN_SEC = 5
    
    #initialize object
    senseHat = SenseHat();
    tempActuator = TempActuatorEmulator.TempActuatorEmulator()

    
    def __init__(self, rateInSec = RATE_IN_SEC):
        super(SenseHatDeviceAdaptor, self).__init__()
        self.rateInSec = rateInSec

    def run(self):
        while True:
            if self.enableEmulator:
                #Generate temperature value
                #curTemp = uniform(float(self.lowVal), float(self.highVal))
                self.curTempPre = self.senseHat.get_temperature_from_pressure()
                self.curTempHum = self.senseHat.get_temperature_from_humidity()
                #add to SensorData
                print("temperature from senseHat pressure :" + str(self.curTempPre))
                print("temperature from senseHat humidity :" + str(self.curTempHum))    
                sleep(self.rateInSec)
                
                