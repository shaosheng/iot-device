'''
Created on Sep 26, 2019

@author: shaoshengyin
'''
import logging
from time import sleep
from labs.module03 import TempSensorAdaptor

tempSensorAdaptor = TempSensorAdaptor.TempSensorAdaptor()
tempSensorAdaptor.enableEmulator = True
tempSensorAdaptor.daemon = True
tempSensorAdaptor.start()
while True:
    sleep(5)
    pass