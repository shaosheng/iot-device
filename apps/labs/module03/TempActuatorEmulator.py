'''
Created on Oct 1, 2019

@author: shaoshengyin
'''
from labbenchstudios.common import ConfigUtil
from labbenchstudios.common import ConfigConst
from labs.module03.SenseHatLedActivator import SenseHatLedActivator
from labs.module03.SimpleLedActivator import SimpleLedActivator
from labs.common.ActuatorData import ActuatorData

class TempActuatorEmulator:
    
    senseHat     = SenseHatLedActivator()
    simpleLED    = SimpleLedActivator()
    actuatorData = ActuatorData()
    
    def __init__(self):
        #load config file
        self.config = ConfigUtil.ConfigUtil('../../../config/ConnectedDevicesConfig.props')
        self.config.loadConfig()        
        #read file and get nominalTemp
        self.nominalTemp = self.config.getProperty(ConfigConst.CONSTRAINED_DEVICE, ConfigConst.NOMINAL_TEMP)
        print("The nomalTemp is = " + str(self.nominalTemp))
        
    def processMessage(self, data):
        self.data = data
        self.actuatorData.updateData(self.data)
        self.senseHat.setEnableLedFlag(True)
        #determine if set msg cold or warm
        if (self.actuatorData.command == 1):
            msg = "warm up"
            self.actuatorData.setStateData(msg)
            message = "actuatorMessage :" + str(self.actuatorData.getValue())
            self.senseHat.setDisplayMessage(message)
            
        if (self.actuatorData.command == 0):
            msg = "cool down"
            self.actuatorData.setStateData(msg)
            #Enable sense_hat run()
            #Send the msg to sense_hat
            message = "actuatorMessage :" + str(self.actuatorData.getValue())
            self.senseHat.setDisplayMessage(message)
            
    def setMessage(self, actuatordata, command, errcode, stateData, statusCode):
        actuatordata.setCommand(command)
        actuatordata.setErrorCode(errcode)
        actuatordata.setStateData(stateData)
        actuatordata.setStatusCode(statusCode)
        
    def check(self, curTemp):
        self.curTemp = curTemp
        self.dif = self.curTemp - float(self.nominalTemp)
        dif = self.dif
        return dif