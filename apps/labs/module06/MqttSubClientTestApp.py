'''
Created on Oct 25, 2019

@author: shaoshengyin
'''

import paho.mqtt.client as mqttClient
from labs.common.DataUtil import DataUtil
import json

mqtt = mqttClient.Client()

def on_connect(client, userdata, flags, resultCode):
    print("Connected with result code "+str(resultCode))
    client.subscribe("test", 2)

def on_message(client, userdata, msg):
    payload = str(msg.payload)                                                      
    print("Received Publish message on topic {0}. ".format(str(msg.topic)))
    if not payload:
        return
    # delete extra signs of message
    payloadtrip = payload.lstrip("b")
    newString = payloadtrip.lstrip("'").rstrip("'")                    
    print("Received message:")
    print(newString)
    dataUtil = DataUtil();
    print("Transfer Json to SensorData instance:")
    # transfer JSON into SensorData instance
    dataFromJava = dataUtil.jsonToSensorData(newString)                           
    print(dataFromJava.__str__()) 
    print("Transfer SensorData instance to JSon:")
    dataUtil.toJson(dataFromJava.name, dataFromJava.timeStamp, dataFromJava.avgValue, dataFromJava.minValue, dataFromJava.maxValue, dataFromJava.curValue, dataFromJava.totValue, dataFromJava.sampleCount)  # transfer SensorData instance to JSON
    print("unsubscribe topic to genreate new packet type")
    #unsubscribe topic
    client.unsubscribe(msg.topic)

def run():
    mqtt.on_connect = on_connect
    mqtt.on_message = on_message
    print("run subscribe from pyhton")
    mqtt.connect("127.0.0.1", 1883, 65)
    mqtt.loop_forever()
    
    
if __name__ == '__main__':  
    run()  
        