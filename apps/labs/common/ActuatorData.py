'''
Created on Oct 1, 2019

@author: shaoshengyin
'''

import os
from datetime import datetime

#declare status code 
COMMAND_OFF   = 0                      # set command_0ff with 0
COMMAND_ON    = 1                      # set command_on with 1
COMMAND_SET   = 2                      # set command_set with 2
COMMAND_RESET = 3                      # set command_reset with 3
STATUS_IDLE   = 0                      # set status_idle with 0
STATUS_ACTIVE = 1                      # set status_active with 1
ERROR_OK              = 0              # set error_ok with 0
ERROR_COMMAND_FAILED  = 1              # set error_command_failed with 1
ERROR_NON_RESPONSIBLE = -1             # set error_non_responsibile with -1

class ActuatorData(object):
    '''
    classdocs
    '''
#set default value
    timeStamp  = None                  
    name       = 'Not set'             
    hasError   = False                 
    command    = 0                     
    errorCode  = 0                     
    statusCode = 0                     
    stateData  = None                  
    val        = 0.0                   


    def __init__(self):
        '''
        Constructor
        '''
        self.updateTimeStamp()
    
    '''
    @return: command
    '''  
    def getCommand(self):
        return self.command
    
    '''
    @return: name
    '''
    def getName(self):
        return self.name
    
    '''
    @return: stateData
    '''
    def getStateData(self):
        return self.stateData
    
    '''
    @return: statusCode
    '''
    def getStatusCode(self):
        return self.statusCode
    
    '''
    @return: errorCode
    '''
    def getErrorCode(self):
        return self.errorCode
    
    '''
    @return: val
    '''
    def getValue(self):
        return self.val;
    
    '''
    @return: hasError
    '''
    def hasError(self):
        return self.hasError    
    
    '''
    @return: command
    '''
    def setCommand(self, command):
        self.command = command
    
    '''
    @return: name
    '''    
    def setName(self, name):
        self.name = name
        
    '''
    set stateData value
    '''
    def setStateData(self, stateData):
        self.stateData = stateData
     
    '''
    set statusCode value
    '''   
    def setStatusCode(self, statusCode):
        self.statusCode = statusCode
        
    '''
    set errorCode value
    '''
    def setErrorCode(self, errCode):
        self.errCode = errCode
        if (self.errCode != 0):        # when errorCode is not 0
            self.hasError = True       # set haserror with true     
        else:
            self.hasError = False      # set haserror with false
            
    '''
    set value
    @param val: value
    '''
    def setValue(self, val):
        self.val = val
    
    '''
    update details
    @param data: data
    '''    
    def updateData(self, data):
        self.command = data.getCommand()        # get command value
        self.statusCode = data.getStatusCode()  # get statusCode value
        self.errCode = data.getErrorCode()      # get errCode value
        self.stateData = data.getStateData()    # get stateData value
        self.val = data.getValue()              # get val value
    
    '''
    update timeStamp
    '''
    def updateTimeStamp(self):
        self.timeStamp = str(datetime.now())    # set tiempStamp
        
    '''
    converts the object and its attributes to a string
    value which will be displayed on console or 
    in trigger email
    @return: customStr
    '''
    def __str__(self):
        customStr = \
            str(self.name + ':' + \
                os.linesep + '\tTime: ' + self.timeStamp + \
                os.linesep + '\tCommand: ' + str(self.command) + \
                os.linesep + '\tStatus Code: ' + str(self.statusCode) + \
                os.linesep + '\tError Code: ' + str(self.errCode) + \
                os.linesep + '\tState Data: ' + str(self.stateData) + \
                os.linesep + '\tValue: ' + str(self.val))
        return customStr
        
    def _dic_(self):
        sensorDict = {
                    'name': self.name,
                    'timeStamp': self.timeStamp, 
                    'command': str(self.command), 
                    'statusCode': str(self.statusCode), 
                    'errCode': str(self.errCode), 
                    'stateData': str(self.stateData), 
                    'Value': str(self.Value)
                    }
        
        return sensorDict
        