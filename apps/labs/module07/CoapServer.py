'''
Created on Oct 28, 2019

@author: shaoshengyin

Ref https://github.com/Tanganelli/CoAPthon
'''
import sys
from coapthon.server.coap import CoAP
from labs.module07.BasicResource import BasicResource

class CoapServer(CoAP):
    def __init__(self, host, port, multicast = False):
        CoAP.__init__(self, (host, port), multicast)
        self.add_resource('basic/', BasicResource())
        print("Coap server start on " + str(host) + ":" + str(port))
        
def usage():
    print("coapserver.py -i <ip address> -p <port>")
    
def main(argv):  # pragma: no cover
    ip = "localhost"
    port = 5683
    multicast = False
    server = CoapServer(ip, port, multicast)
    try:
        server.listen(10)
    except KeyboardInterrupt:
        print("Server Shutdown")
        server.close()
        print("Exiting...")


if __name__ == "__main__":  # pragma: no cover
    main(sys.argv[1:])       