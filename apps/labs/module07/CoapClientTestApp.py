'''
Created on Oct 28, 2019

@author: shaoshengyin
'''
import sys
sys.path.append('/home/pi/yin/iot-device/apps')
from labs.common.SensorData import SensorData
from labs.common.DataUtil import DataUtil
from coapthon.client.helperclient import HelperClient

# initialize the host, post and path
host = "192.168.99.190"
port = 5683
path = 'temp'

# initialize sensorData and DataUtil
sensorData = SensorData()
dataUtil = DataUtil()

#configure value
sensorData.addValue(18)
sensorData.setName("name")
#transform to json
dataJson = dataUtil.toJson(sensorData.name, sensorData.timeStamp, sensorData.avgValue, sensorData.minValue, sensorData.maxValue, sensorData.curValue, sensorData.totValue, sensorData.sampleCount)
print("Json format of sensorData: " + dataJson)

#set up connection
client = HelperClient(server=(host, port))
print("Ping Successful!")

#test get method
response = client.get(path)
print("Testing get handler")
print(response.pretty_print())

#test post method
response = client.post(path, dataJson)
print("Testing post handler")
print(response.pretty_print())

#test put method
response = client.put(path, dataJson)
print("Testing put handler")
print(response.pretty_print())

#test delete method
response = client.delete(path)
print("Testing get handler")
print(response.pretty_print())

client.stop()