'''
Created on Oct 28, 2019

@author: shaoshengyin

Ref: https://github.com/Tanganelli/CoAPthon
'''

from coapthon.resources.resource import Resource

class BasicResource(Resource):
    def __init__(self, name="BasicResource", coap_server=None):
        super(BasicResource, self).__init__(name, coap_server, visible=True,
                                            observable=True, allow_children=True)
        self.payload = "Basic Resource"
    
    #get handler
    def render_GET(self, request):
        return self
    
    #put handler
    def render_PUT(self, request):
        self.payload = request.payload
        return self
    
    #post handler
    def render_POST(self, request):
        res = BasicResource()
        res.location_query = request.uri_query
        res.payload = request.payload
        return res
    
    #post delete handler
    def render_DELETE(self, request):
        return True