'''
Created on Sep 17, 2019

@author: shaoshengyin
'''
import psutil
import logging

class SystemMemUtilTask(object):
    
    #constructor
    def __init__(self):
        pass
        
    #Return a float
    #Percentage of Memory Utilization
    def getDataFromSensor(self):
        sysMem = psutil.virtual_memory()
        return float(sysMem.percent)        
        