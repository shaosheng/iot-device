'''
Created on Sep 17, 2019

@author: shaoshengyin
'''

import threading
import logging
from time import sleep
from labs.module01.SystemCpuUtilTask import SystemCpuUtilTask
from labs.module01.SystemMemUtilTask import SystemMemUtilTask

class SystemPerformanceAdaptor(threading.Thread):
    
    DEFAULT_RATE = 5    
    
    #constructor, initialize two tasks class
    #para rateInSec: the interval of thread
    def __init__(self, rateInSec = DEFAULT_RATE):
        super(SystemPerformanceAdaptor, self).__init__();
        self.rateInSec = rateInSec
        self.sysCpuUtilTask = SystemCpuUtilTask()
        self.sysMemUtilTask = SystemMemUtilTask()

    #override thread run method
    #display result in logger
    def run(self):
        while True:
            if self.enableAdaptor:
                CpuUtil = self.sysCpuUtilTask.getDataFromSensor()
                MemUtil = self.sysMemUtilTask.getDataFromSensor()
                logging.info("CPU Utilizaiton = " + str(CpuUtil))
                logging.info("Memory Utilizaiton = " + str(MemUtil))
                sleep(self.rateInSec)
        
        