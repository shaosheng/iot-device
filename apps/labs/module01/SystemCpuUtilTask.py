'''
Created on Sep 17, 2019

@author: shaoshengyin
'''

import psutil
import logging

class SystemCpuUtilTask(object):
    
    #constructor
    def __init__(self):
        pass
    
    #Return a float
    #Percentage of CPU Utilization    
    def getDataFromSensor(self):
        sysCpu = psutil.cpu_percent(1.0, False)
        return float(sysCpu)        