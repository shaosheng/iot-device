'''
Created on Sep 17, 2019

@author: shaoshengyin
'''
import logging
from time import sleep
from labs.module01 import SystemPerformanceAdaptor

#logger config
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.INFO)

#Initialize SystemPerformanceAdaptor
#Daemon default value is false, set true
#A daemon thread runs without blocking the main program from exiting. And when main program exits, associated daemon threads are killed too. 
#set interval 3 sec
sysPerfAdaptor = SystemPerformanceAdaptor.SystemPerformanceAdaptor(3)
sysPerfAdaptor.daemon = True
sysPerfAdaptor.enableAdaptor = True
sysPerfAdaptor.start()
while (True):
    #keep main thread alive 
    #close thread when stop manually
    sleep(5)
    pass